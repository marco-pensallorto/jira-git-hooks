**JIRA Git hooks**

These git hooks can be used as helper scripts to streamline our day-to-day workflow.

---

## Install

To install the hooks for a particular git repository you'll have to **clone** this repo into <your-repo-name>/.git/hooks.

---

