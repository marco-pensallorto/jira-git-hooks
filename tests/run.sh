#!/bin/bash
PROGRAM="../commit-msg"

run_test() {
    FILENAME=$1
    echo -n "-- $FILENAME: "
    &> /dev/null $PROGRAM $FILENAME
}

check_acceptance() {
    run_test $1
    if [ $? -eq 0 ];
    then
        echo "OK";
    else
        echo "KO";
    fi
}

check_rejection() {
    run_test $1
    if [ $? -eq 0 ];
    then
        echo "KO";
    else
        echo "OK";
    fi
}

check_acceptance good1.txt
check_acceptance good2.txt
check_acceptance good3.txt

check_rejection bad1.txt
check_rejection bad2.txt
check_rejection bad3.txt
check_rejection bad4.txt
check_rejection bad5.txt

